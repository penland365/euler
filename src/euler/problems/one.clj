(ns euler.problems.one
  (:require [clojure.set :refer[union]]
            [clojure.term.colors :refer [on-blue bold yellow green on-green on-grey]])
  (:gen-class))

(defn floor-of-upper-limit
  "Takes 'n', 'u', determines the floor of the upper limit of u / n"
  [n u]
  (assert (instance? Long n))
  (assert (instance? Long u))
  (long (Math/floor (/ u n))))

(defn make-multiples
  [n k limit c]
  (if (> k limit)
    (set c)
    (let [j (* n k)]
      (recur n (+ k 1) limit (conj c j)))))

(defn answer
  "Builds the answer for the givein limit."
  [limit]
  (assert (instance? Long limit))
  (assert (< 0 limit))
  (let [three-upper-lim (floor-of-upper-limit 3 limit)
        three-coll      (make-multiples 3 1 three-upper-lim [])
        five-upper-lim  (floor-of-upper-limit 5 limit)
        five-coll       (make-multiples 5 1 five-upper-lim [])]
    (->> (union three-coll five-coll)
         vec
         (reduce + 0))))

(defn run 
  "Problem: 
    If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
    Find the sum of all the multiples of 3 or 5 below 1000."
  [limit]
  (let [result (answer limit)]
    (println (on-blue "Definition:\nIf we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.\nFind the sum of all the multiples of 3 or 5 below 1000."))
    (println (bold (green "Answer:  " result)))))
