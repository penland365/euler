(ns euler.problems.template
  (:require [clojure.term.colors :refer [on-blue bold green]])
  (:gen-class))

(def problem
  "")

(defn answer
  ""
  []
  "???")

(defn run 
  "Problem:"
  [n]
  (println (on-blue (format "Definition:\n%s" problem)))
  (let [result (answer n)]
    (println (bold (green "Answer:  " result)))))
