(ns euler.problems.three
  (:require [clojure.term.colors :refer [on-blue bold yellow green on-green on-grey]]
            [euler.algos.integers :refer [floor sqrt]]
            [euler.algos.primes :refer [eratosthenes-sieve]])
  (:gen-class))

(def problem
  "The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143?")

(defn answer
  ""
  [n]
  (let [sieve (-> (sqrt n)
                  floor
                  eratosthenes-sieve)]
    (->> (filter (fn [x] (= 0 (mod n x))) sieve)
         last)))

(defn run 
  "Problem:
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143?"
  [n]
  (println (on-blue (format "Definition:\n%s" problem)))
  (let [result (answer n)]
    (println (bold (green "Answer:  " result)))))
