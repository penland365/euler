(ns euler.core
  (:require [clojure.term.colors :refer [on-red]]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.string :as string]
            [euler.problems.one :as one]
            [euler.problems.two :as two]
            [euler.problems.three :as three])
  (:gen-class))

(def problems 
  {"1" {:f one/run :args [999] :test-args [9] :description "Problem 1"}
   "2" {:f two/run :args [4000000] :test-args [100] :description "Problem 2"}
   "3" {:f three/run :args [600851475143] :test-args [13195] :description "Problem 3"}})

(def cli-options
  [["-t" "--test" "Pass test arguments for each problems as defined in the code."]
   ["-h" "--help"]])

(defn run-all
  "Run all problems defined in runs."
  [test?]
  (println (on-red "Running all Euler problems sequentially from 1 to N."))
  (doall (map (fn [x]
                (println)
                (println (on-red (:description x)))
                (let [problem-args (if test? (:test-args x) (:args x))]
                  (when test?
                    (println (on-red "Running with test flag enabled, test args = " (first problem-args))))
                  (apply (:f x) (:args x))))
             (vec (vals problems)))))

(defn usage [options-summary]
  (->> ["Project Euler solutions. https://projecteuler.net/"
        ""
        "Usage: euler [options] problem"
        ""
        "Options:"
        options-summary
        ""
        "Problems:"
        "  all  Runs the solutions for all problems from 1 to N"
        ""
        "  one   Multiples of 3 or 5"
        "  two   Even Fibanacci Numbers" 
        "  three Largest Prime Factor"
        ""
        "Please refer to the code documentation for each problem for more information."]
       (string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with an error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options)         {:exit-message (usage summary) :ok? true}
      (= 1 (count arguments)) {:problem (first arguments) :options options}
      :else {:exit-message (usage summary)})))

(defn exit [status msg]
  (if (= 0 status)
    (println msg)
    (binding [*out* *err*]
      (println (on-red msg))))
  (System/exit status))
           
(defn -main
  ""
  [& args]
  (let [{:keys [problem options exit-message ok?] :as all} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (let [test? (true? (:test options))]
        (case problem
          "all" (run-all test?)
          "All" (run-all test?)
          "ALL" (run-all test?)
          (let [p (get problems problem)]
            (if (nil? p)
              (exit 1 (format "ERROR: unsolved problem %s requested." problem ))
              (let [problem-args (if test? (:test-args p) (:args p))]
                (when test?
                  (println (on-red "Running with test flag enabled, test args = " (first problem-args) "\n")))
                (apply (:f p) problem-args)))))))))
