(ns euler.algos.integers
  "Common integer algorithms."
  (:gen-class))

(defn floor
  "Takes 'x', returns the greatest integer less than or equal to x."
  [x]
  (assert (not (nil? x)) "Argument x cannot be nil, must be a number.")
  (assert (number? x) "Argument x must be a number")
  (if (integer? x)
    x
    (int (Math/floor x))))

(defn sqrt
  "Takes 'x', returns the square root of x."
  [x]
  (assert (not (nil? x)) "Argument x cannot be nil, must be a number.")
  (assert (number? x) "Argument x must be a number")
  (Math/sqrt x))
