(ns euler.algos.primes
  "Common prime number algorithms."
  (:gen-class))

(defn eratosthenes-sieve 
  "Takes 'x', generates a vector of all primes less than or equal to x via Eratosthenes algorithm.

   Uses Eratosthenes algorithm."
  [x]
  (assert (not (nil? x)) "Argument x cannot be nil, must be an integer.")
  (assert (integer? x) "Argument x must be an integer.")
  
  ;; build out initial table
  (let [initial-table (loop [n      2
                             result {}]
                        (if (<= n x)
                          (recur (+ n 1) (assoc result n {:prime? true :checked? false}))
                          result))]

    ;; outer loop from 2 to X
    (let [result (loop [n            2
                        sieved-table initial-table]
                   (if (<= n x)

                     ;; inner loop from i to X
                     (let [composites-of-n (loop [i  2
                                                  xs sieved-table]
                                             (let [j (* i n)]
                                               (if (<= j x)
                                                 (recur (+ 1 i) (update xs j (fn [_] {:prime? false :checked? true})))
                                                 xs)))]
                       (recur (+ 1 n) (update-in composites-of-n [n :checked?] (fn [_] true))) )

                     sieved-table))]

      ;; filter out non-primes, sort the list, convert to a vector
      (->> (filter (fn [y] (:prime? (val y))) result)
           (map #(key %))
           sort
           vec))))
