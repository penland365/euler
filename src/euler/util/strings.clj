(ns euler.util.strings
  "Common string utilities."
  (:require [euler.algos.integers :refer [floor]])
  (:gen-class))

(defn palindrome? 
  "Takes 's', determines if `s` is a palindrome. Throws AssertionError is `s` is not a string."
  [s]
  (assert (string? s) "Argument s must be a string.")
  (let [s-len (count s)]
    (cond
      (= 0 s-len) false
      (= 1 s-len) true
      :else       (loop [idx       0
                         limit-idx (if (even? (mod s-len 2)) (- (/ s-len 2) 1) (floor (/ s-len 2)))
                         p?        true]
                    (cond
                      (not p?)          p?
                      (> idx limit-idx) p?
                      :else             (let [i-start-idx idx 
                                              i-end-idx   (+ 1 i-start-idx)
                                              i           (subs s i-start-idx i-end-idx)
                                              j-end-idx   (- s-len idx)
                                              j-start-idx (- j-end-idx 1)
                                              j           (subs s j-start-idx j-end-idx)
                                              equal?      (= i j)]
                                          (recur (+ 1 idx) limit-idx equal?)))))))
