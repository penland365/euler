(ns euler.problems.template-test
  (:require [clojure.test :refer :all]
            [euler.problems.template :refer :all]))

(deftest answer-test
  (testing "returns the correct answer for the problems definition"
    (is (= "???" (answer)))))

