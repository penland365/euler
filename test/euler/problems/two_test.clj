(ns euler.problems.two-test
  (:require [clojure.test :refer :all]
            [euler.problems.two :refer :all]))

(deftest answer-tests
  (testing "throws assertion error if j is nil"
    (is (thrown? AssertionError (answer nil 2 100 0))))
  (testing "throws assertion error if j is not a number"
    (is (thrown? AssertionError (answer "asdf" 2 100 0))))
  (testing "throws assertion error if j is not a whole number"
    (is (thrown? AssertionError (answer 3.14 2 100 0))))
  (testing "throws assertion error if k is nil"
    (is (thrown? AssertionError (answer 1 nil 100 0))))
  (testing "throws assertion error if k is not a number"
    (is (thrown? AssertionError (answer 1 "asdf" 100 0))))
  (testing "throws assertion error if k is not a whole number"
    (is (thrown? AssertionError (answer 1 3.14 100 0))))
  (testing "throws assertion error if limit is nil"
    (is (thrown? AssertionError (answer 1 2 nil 0))))
  (testing "throws assertion error if limit is not a number"
    (is (thrown? AssertionError (answer 1 2 "asdf" 0))))
  (testing "throws assertion error if limit is not a whole number"
    (is (thrown? AssertionError (answer 1 2 3.14 0))))
  (testing "throws assertion error if sum is nil"
    (is (thrown? AssertionError (answer 1 2 100 nil))))
  (testing "throws assertion error if sum is not a number"
    (is (thrown? AssertionError (answer 1 2 100 "asdf"))))
  (testing "throws assertion error if sum is not a whole number"
    (is (thrown? AssertionError (answer 1 2 100 3.14))))
  (testing "has correct answer for test case of 100"
    (is (= 44 (answer 1 2 100 0)))))
