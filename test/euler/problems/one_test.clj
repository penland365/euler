(ns euler.problems.one-test
  (:require [clojure.test :refer :all]
            [euler.problems.one :refer :all]))

(deftest floor-of-upper-limit-tests
  (testing "throws assertion error if n is nil"
    (is (thrown? AssertionError (floor-of-upper-limit nil 10))))
  (testing "throws assertion error if n is not a Long"
    (is (thrown? AssertionError (floor-of-upper-limit "foobar" 10))))
  (testing "throws assertion error if u is nil"
    (is (thrown? AssertionError (floor-of-upper-limit 3 nil))))
  (testing "throws assertion error if u is not a Long"
    (is (thrown? AssertionError (floor-of-upper-limit 3 "foobar"))))
  (testing "result is a long"
    (is (instance? Long (floor-of-upper-limit 3 10))))
  (testing "returns correct result for a limit of 10 and a N of 3"
    (is (= 3 (floor-of-upper-limit 3 10))))
  (testing "returns correct result for a limit of 9 and a N of 5"
    (is (= 1 (floor-of-upper-limit 5 9))))
  (testing "returns correct result for a limit of 10 and a N of 5"
    (is (= 2 (floor-of-upper-limit 5 10)))))

(deftest answer-tests
  (testing "throws assertion error if limit is nil"
    (is (thrown? AssertionError (answer nil))))
  (testing "throws assertion error if limit is not a number"
    (is (thrown? AssertionError (answer "asdf"))))
  (testing "throws assertion error if limit is not a whole number"
    (is (thrown? AssertionError (answer 3.14))))
  (testing "throws assertion error if limit is not a whole number"
    (is (thrown? AssertionError (answer 3.14))))
  (testing "throws assertion error if limit is not greater than 0"
    (is (thrown? AssertionError (answer -1)))
    (is (thrown? AssertionError (answer 0))))
  (testing "has correct answer for test case of 9"
    (is (= 23 (answer 9)))))

