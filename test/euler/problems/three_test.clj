(ns euler.problems.three-test
  (:require [clojure.test :refer :all]
            [euler.problems.three :refer :all]))

(deftest answer-test
  (testing "returns the correct answer for the problems definition of 29" 
    (= 29 (answer 13195))))

