(ns euler.util.strings-test
  (:require [clojure.test :refer :all]
            [euler.util.strings :refer :all]))

(deftest palindrome-tests
  (testing "throws AssertionError if s is nil"
    (is (thrown? AssertionError (palindrome? nil))))
  (testing "throws AssertionError if s is an int"
    (is (thrown? AssertionError (palindrome? 71))))
  (testing "returns false is s is empty"
    (is (false? (palindrome? ""))))
  (testing "returns false is s is of length 1"
    (is (true? (palindrome? "1")))
    (is (true? (palindrome? "2"))))
  (testing "returns true is s is a palindrome"
    (is (true? (palindrome? "ABA")))
    (is (true? (palindrome? "ABBA")))
    (is (true? (palindrome? "ABBBA")))
    (is (true? (palindrome? "9009"))))
  (testing "returns false is s is NOT a palindrome"
    (is (false? (palindrome? "foo")))
    (is (false? (palindrome? "foo bar")))))
