(ns euler.algos.primes-test
  (:require [clojure.test :refer :all]
            [euler.algos.primes :refer :all]))

(deftest eratosthenessieve-assertionerror-tests
  (testing "throws AssertionError if x is nil"
    (is (thrown? AssertionError (eratosthenes-sieve nil))))
  (testing "throws AssertionError if x is a string"
    (is (thrown? AssertionError (eratosthenes-sieve "foo"))))
  (testing "throws AssertionError if x is a map"
    (is (thrown? AssertionError (eratosthenes-sieve {}))))
  (testing "throws AssertionError if x is a vec"
    (is (thrown? AssertionError (eratosthenes-sieve []))))
  (testing "throws AssertionError if x is a float or double"
    (is (thrown? AssertionError (eratosthenes-sieve 3.14)))))

(deftest eratosthenessieve-tests
  (testing "returns valid primes under 10"
    (= [2 3 5 7] (eratosthenes-sieve 10))))
