(ns euler.algos.integers-test
  (:require [clojure.test :refer :all]
            [euler.algos.integers :refer :all]))

(deftest floor-assertionerror-tests
  (testing "throws AssertionError if x is nil"
    (is (thrown? AssertionError (floor nil))))
  (testing "throws AssertionError if x is a string"
    (is (thrown? AssertionError (floor "foo"))))
  (testing "throws AssertionError if x is a map"
    (is (thrown? AssertionError (floor {}))))
  (testing "throws AssertionError if x is a vec"
    (is (thrown? AssertionError (floor [])))))

(deftest floor-tests
  (testing "returns x when x is an integer"
    (is (= 71 (floor 71))))
  (testing "returns x when x is an integer"
    (is (= 0 (floor 0))))
  (testing "returns the greatest integer less than x"
    (is (= 71 (floor 71.71))))
  (testing "returns the greatest integer less than x"
    (is (= -72 (floor -71.71)))))

(deftest sqrt-assertionerror-tests
  (testing "throws AssertionError if x is nil"
    (is (thrown? AssertionError (sqrt nil))))
  (testing "throws AssertionError if x is a string"
    (is (thrown? AssertionError (sqrt "foo"))))
  (testing "throws AssertionError if x is a map"
    (is (thrown? AssertionError (sqrt {}))))
  (testing "throws AssertionError if x is a vec"
    (is (thrown? AssertionError (sqrt [])))))

(deftest sqrt-tests
  (testing "returns the square root of x"
    (is (= 3.0 (sqrt 9)))))
